/*
* 카이앤으로 인하여 속도 느림 현상
*/

#define CAYENNE_DEBUG
#define CAYENNE_PRINT Serial

#include <Timer5.h>
#include <CayenneMQTTMKR1000.h>

#define VOICE_RECOGNITION_READY_PIN 0
#define VOICE_RECOGNITION_UP_PIN 5
#define VOICE_RECOGNITION_DOWN_PIN 7

#define CDS_SENSOR_PIN A1
#define CDS_VIRTUAL_CHANNEL 1

#define UP_VIRTUAL_CHANNEL 2
#define UP_ACTUATOR_PIN 6

#define DOWN_VIRTUAL_CHANNEL 3
#define DOWN_ACTUATOR_PIN 10

#define AUTO_ACTUATOR_PIN 8

#define MOTOR_SPEED 750

#define TRIGGER_CH1_PIN  11 //첫번째 Trigger pin
#define ECHO_CH1_PIN     12 //첫번째 Echo pi

#define TRIGGER_CH2_PIN  2 //두번째 Trigger pin
#define ECHO_CH2_PIN     1 //두번째 Echo Pin

bool MNU = HIGH;
bool motorContrl = LOW;

const char ssid[] = "miro";
const char wifiPassword[] = "miro8025";

const char username[] = "fff75320-2bd4-11e8-82f6-390cc0260849";
const char password[] = "2396f57fbdda82326e3f1bb73b64e87b6b170431";
const char clientID[] = "1474bba0-6acb-11e8-ba3c-d96ece977676";

const signed char stp = 4;
const signed char dir = 3;

int mAverageUltrasonicUP[3] = { 0, };
int mAverageUltrasonicUPAddress = 0;
int mSumUltrasonicUP = 0;

int mAverageUltrasonicDOWN[3] = { 0, };
int mAverageUltrasonicDOWNAddress = 0;
int mSumUltrasonicDOWN = 0;

signed char upDownStats = 0; // 2 == auto

float sench1 = GetAvg(getUltrasonicLenght(TRIGGER_CH1_PIN, ECHO_CH1_PIN), &mSumUltrasonicUP, mAverageUltrasonicUP, &mAverageUltrasonicUPAddress);
float sench2 = GetAvg(getUltrasonicLenght(TRIGGER_CH2_PIN, ECHO_CH2_PIN), &mSumUltrasonicDOWN, mAverageUltrasonicDOWN, &mAverageUltrasonicDOWNAddress);

void setup()
{
	Serial.begin(9600);
	pinMode(stp, OUTPUT);
	pinMode(dir, OUTPUT);
	pinMode(UP_ACTUATOR_PIN, INPUT_PULLUP);
	pinMode(AUTO_ACTUATOR_PIN, INPUT_PULLUP);
	pinMode(DOWN_ACTUATOR_PIN, INPUT_PULLUP);
	digitalWrite(stp, LOW);

	pinMode(TRIGGER_CH1_PIN, OUTPUT);
	pinMode(ECHO_CH1_PIN, INPUT);
	pinMode(TRIGGER_CH2_PIN, OUTPUT);
	pinMode(ECHO_CH2_PIN, INPUT);

	pinMode(UP_ACTUATOR_PIN, OUTPUT);
	pinMode(DOWN_ACTUATOR_PIN, OUTPUT);

	pinMode(VOICE_RECOGNITION_READY_PIN, INPUT_PULLUP);
	pinMode(VOICE_RECOGNITION_UP_PIN, INPUT_PULLUP);
	pinMode(VOICE_RECOGNITION_DOWN_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(VOICE_RECOGNITION_READY_PIN), VoiceRecognitionReady, RISING);

	MyTimer5.begin(MOTOR_SPEED);
	MyTimer5.attachInterrupt(MotorPulse);
	MyTimer5.start();

	Cayenne.begin(username, password, clientID, ssid, wifiPassword);
	analogWrite(A2, 1024);
}

void loop()
{
	Cayenne.loop();
	sench1 = GetAvg(getUltrasonicLenght(TRIGGER_CH1_PIN, ECHO_CH1_PIN), &mSumUltrasonicUP, mAverageUltrasonicUP, &mAverageUltrasonicUPAddress);
	Serial.print(sench1);
	Serial.print(" ");
	Serial.println(sench2);
	Serial.println(upDownStats);
	if (digitalRead(AUTO_ACTUATOR_PIN) == LOW)
	{
		int cdsValue = analogRead(CDS_SENSOR_PIN);
		if (sench1 < 15 && cdsValue < 500)
		{
			digitalWrite(dir, LOW);
			motorContrl = HIGH;
		}
		else if (sench2 > 15 && cdsValue > 500)
		{
			digitalWrite(dir, HIGH);
			motorContrl = HIGH;
		}
		else
		{
			motorContrl = LOW;
			digitalWrite(stp, LOW);
		}
	}
	else if (digitalRead(AUTO_ACTUATOR_PIN) == HIGH)
	{
		if ((upDownStats == 1 || digitalRead(UP_ACTUATOR_PIN) == LOW) && sench1 < 15)
		{
			digitalWrite(dir, LOW);
			motorContrl = HIGH;
		}
		else if ((upDownStats == -1 || digitalRead(DOWN_ACTUATOR_PIN) == LOW) && sench2 > 15)
		{
			digitalWrite(dir, HIGH);
			motorContrl = HIGH;
		}
		else
		{
			motorContrl = LOW;
			digitalWrite(stp, LOW);
		}
	}
	sench2 = GetAvg(getUltrasonicLenght(TRIGGER_CH2_PIN, ECHO_CH2_PIN), &mSumUltrasonicDOWN, mAverageUltrasonicDOWN, &mAverageUltrasonicDOWNAddress);

}

void MotorPulse()
{
	if (motorContrl == HIGH)
	{
		digitalWrite(stp, MNU = !MNU);
	}
	else
	{
		digitalWrite(stp, LOW);
	}
}

float getUltrasonicLenght(const int trigerPin, const int echoPin)
{
	digitalWrite(trigerPin, LOW);
	digitalWrite(echoPin, LOW);
	delayMicroseconds(2);
	digitalWrite(trigerPin, HIGH);
	delayMicroseconds(10);
	digitalWrite(trigerPin, LOW);
	return (((float)(340 * pulseIn(echoPin, HIGH, 15000)) / 10000) / 2);
}

float GetAvg(float value, int *sum, int *array, int *count)
{
	int size = sizeof(array) / sizeof(int);
	if (*count == size)
	{
		*count = 0;
	}
	*sum -= *(array + *count);
	*(array + *count) = (int)value;
	*sum += *(array + *count);
	(*count)++;
	return *sum / size;
}

CAYENNE_OUT(CDS_VIRTUAL_CHANNEL)
{
	Cayenne.virtualWrite(CDS_VIRTUAL_CHANNEL, analogRead(CDS_SENSOR_PIN));
}

CAYENNE_IN(UP_VIRTUAL_CHANNEL)
{
	int value = getValue.asInt();
	CAYENNE_LOG("Channel %d, pin %d, value %d", UP_VIRTUAL_CHANNEL, UP_ACTUATOR_PIN, value);
	if (value == 0)
	{
		upDownStats = 1;
	}
	else
	{
		upDownStats = 0;
	}
}

CAYENNE_IN(DOWN_VIRTUAL_CHANNEL)
{
	int value = getValue.asInt();
	CAYENNE_LOG("Channel %d, pin %d, value %d", DOWN_VIRTUAL_CHANNEL, DOWN_ACTUATOR_PIN, value);
	// Write the value received to the digital pin.
	if (value == 0)
	{
		upDownStats = -1;
	}
	else
	{
		upDownStats = 0;
	}
}

void VoiceRecognitionReady()
{
	int timeOut = 5000000; //10s
	upDownStats = 0;
	analogWrite(A3, 1024);
	while (timeOut--)
	{
		if (digitalRead(VOICE_RECOGNITION_UP_PIN) == HIGH)
		{
			upDownStats = 1;
			break;
		}
		else if(digitalRead(VOICE_RECOGNITION_DOWN_PIN) == HIGH)
		{
			upDownStats = -1;
			break;
		}
	}
	analogWrite(A3, 0);
}
